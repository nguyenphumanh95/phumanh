import java.util.Scanner;

import javax.tools.JavaCompiler;

import java.math.*;

public class Main 
{
	
	public static boolean CheckSoCP(int n) 
	{
		int i = (int)Math.sqrt(n);
		if(n == Math.pow(i, 2))
		{
			return true;
		}
		return false;
	}
	
	public static boolean CheckSoNT(int n)
	{
		int dem = 0;
		for(int i = 1; i <= n; i++)
		{
			
			if(n % i == 0)
			{
				dem++;
			}
		}
		
		if(dem == 2)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static boolean CheckChuoi(String n)
	{
		for(int i = 0; i <= n.length()/2; i++)
		{
			if(n.charAt(i) == n.charAt(n.length() - i - 1))
			{
				return true;
			}
		}
		return false;
	}
	
	public static void main(String[] args) 
	{
		Scanner scanln = new Scanner(System.in);
		/*
		//B�i 5
		int[] a = new int[100];
		
		for(int i = 0; i < 100; i++)
		{
			a[i] = 100 - i;
		}
		
		System.out.println("Mang truoc khi sap xep:");
		
		for(int i = 0; i <100; i++)
		{
			System.out.println(a[i]);	
		}
		
		for(int i = 0; i < 100; i++)
		{
			for(int j = 0; j < 100; j++)
			{
				if(a[i] < a[j])
				{
					int temp;
					temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}
			}
		}
		
		System.out.println("Mang sau khi sap xep:");
		
		for(int i = 0; i <100; i++)
		{
			System.out.println(a[i]);	
		}
		*/
		
		/*
		//B�i 6
		System.out.print("Xin moi nhap so phan tu cua mang: ");
		int n = scanln.nextInt();
		int[] arr = new int[n];
		
		for(int i = 0; i < n; i++)
		{
			System.out.print("Xin moi nhap phan tu thu arr[");
			System.out.print(i);
			System.out.print("]: ");
			arr[i] = scanln.nextInt();
		}
		
		System.out.print("Cac so chinh phuong va so nguyen to la: ");	
		for(int i = 0; i < n; i++)
		{
			if(CheckSoCP(arr[i]) || CheckSoNT(arr[i]))
			{
				System.out.print(arr[i]);
				System.out.print("\t");
			}
		}
		*/
		
		
		//B�i 7
		System.out.print("Xin moi nhap chuoi: ");
		String n = scanln.nextLine();
		
		if(CheckChuoi(n))
		{
			System.out.print("Chuoi nay la chuoi doi xung");
		}
		else
		{
			System.out.print("Chuoi nay khong phai la chuoi doi xung");
		}
		
		
		//B�i 8
		
		scanln.close();
	}
}

