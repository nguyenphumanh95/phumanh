package BaiTapVeClass.src;

public class BubbleSort extends SapXep
{
	@Override
	public void Sort(int[] a)
	{
		for(int i = 0; i < a.length; i++)
		{
			for(int j = a.length - 1; j > 0; j--)
			{
				if(a[j] > a[j - 1])
				{
					int temp;
					temp = a[j];
					a[j] = a[j - 1];
					a[j - 1] = temp;
				}
			}
		}
	}
}