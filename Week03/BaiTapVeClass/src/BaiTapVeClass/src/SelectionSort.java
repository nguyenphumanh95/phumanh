package BaiTapVeClass.src;

public class SelectionSort extends SapXep
{
	public void Sort(int[] a)
	{
		for(int i = 0; i < a.length; i++)
		{
			int max = a[i];
			int index = i;
			
			for(int j = i + 1; j < a.length; j++)
			{
				if(max < a[j])
				{
					max = a[j];
					index = j;
				}
			}
			
			if (index != i)
			{
				int temp;
				temp = a[i];
				a[i] = a[index];
				a[index] = temp;
			}
		}
	}
}