package BaiTapVeClass.src;

import java.util.Random;

public class Main 
{
	public static void output(int a[]) 
	{
		if (null == a)
		{
			return;
		}
		else
		{
			for(int i = 0; i < a.length; i++) 
			{
				System.out.print(a[i] + ", ");
			}
			
			System.out.println();
		}
	}
	
	public static void main(String[] args)
	{
		int a[] = new int[10];
		Random random = new Random();
		SapXep sort;
		
		System.out.println("Selection Sort:");
		
		for(int i = 0; i < a.length; i++) 
		{
			a[i] = random.nextInt(100);
		}
		
		output(a);
		sort = new SelectionSort();
		sort.Sort(a);
		output(a);
		
		System.out.println("Bubble Sort:");
		
		for(int i = 0; i < a.length; i++) 
		{
			a[i] = random.nextInt(100);
		}
		
		output(a);
		sort = new BubbleSort();
		sort.Sort(a);
		output(a);
	}
}

